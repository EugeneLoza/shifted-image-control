function MakeAlphaBleed(const Src: {$ifdef rgba}TRGBAlphaImage{$else}TGrayscaleAlphaImage{$endif}): {$ifdef rgba}TRGBAlphaImage{$else}TGrayscaleAlphaImage{$endif};
const
  AlphaChannel = {$ifdef rgba}3{$else}1{$endif};
var
  DstP, SrcP: {$ifdef rgba}PVector4Byte{$else}PVector2Byte{$endif};
  SrcA: Byte;
  X, Y, Z, Dist: Integer;
  W, H: Integer;
  AvgColor: {$ifdef rgba}TVector4{$else}TVector2{$endif};
  ColorFound: Boolean;

  procedure FindColor;
  var
    DX, DY: Integer;
  begin
    for DX := -Dist to Dist do
      if (X + DX >= 0) and (X + DX < W) then
        for DY := -Dist to Dist do
          if (Y + DY >= 0) and (Y + DY < H) then
            if Sqr(DX) + Sqr(DY) <= Sqr(Dist) then
            begin
              SrcP := Src.PixelPtr(X + DX, Y + DY, Z);
              SrcA := SrcP^.Data[AlphaChannel];
              if SrcA > Threshold then
              begin
                if not ColorFound then
                begin
                  ColorFound := true;
                  Exit;
                end;
                AvgColor.Data[0] := AvgColor.Data[0] + Single(SrcP^.Data[0]) * Single(SrcA);
                {$ifdef rgba}
                AvgColor.Data[1] := AvgColor.Data[1] + Single(SrcP^.Data[1]) * Single(SrcA);
                AvgColor.Data[2] := AvgColor.Data[2] + Single(SrcP^.Data[2]) * Single(SrcA);
                {$endif}
                AvgColor.Data[AlphaChannel] := AvgColor.Data[AlphaChannel] + Single(SrcA);
              end;
            end;
  end;

begin
  Result := Src.MakeCopy as {$ifdef rgba}TRGBAlphaImage{$else}TGrayscaleAlphaImage{$endif};
  W := Src.Width;
  H := Src.Height;
  for X := 0 to W - 1 do
  begin
    for Y := 0 to H - 1 do
      for Z := 0 to Src.Depth - 1 do
      begin
        DstP := Result.PixelPtr(X, Y, Z);
        if DstP^.Data[AlphaChannel] <= Threshold then
        begin
          Dist := 0;
          ColorFound := false;
          repeat
            Inc(Dist);
            FindColor;
          until ColorFound or (Dist > MaxDistance) or ((Dist > W) and (Dist > H));
          if ColorFound then
          begin
            Dist := Dist + Trunc(Sqrt(Dist)) - 1; //min = 1 + 1 - 1
            AvgColor := {$ifdef rgba}TVector4{$else}TVector2{$endif}.Zero;
            FindColor;
            DstP^.Data[0] := Clamped(Trunc(AvgColor.Data[0] / AvgColor.Data[AlphaChannel]), 0, 255);
            {$ifdef rgba}
            DstP^.Data[1] := Clamped(Trunc(AvgColor.Data[1] / AvgColor.Data[AlphaChannel]), 0, 255);
            DstP^.Data[2] := Clamped(Trunc(AvgColor.Data[2] / AvgColor.Data[AlphaChannel]), 0, 255);
            {$endif}
          end else
          begin
            if (Dist > W) and (Dist > H) then
            begin
              WriteLn('Could not find a single pixel with alpha higher than treshold (' + Threshold.ToString + '). Nothing changed, the image will be re-saved as-is.');
              Exit;
            end else
              //Note that if MaxDistance < W or MaxDistance < H "image empty" exit will never happen, but the result will be the same in case the image is actually empty.
              Continue;
          end;
        end;
      end;
    if X mod (W div 10) = 0 then
      WriteLn((10 * Round(10 * X / (W - 1))).ToString + ' %');
  end;
end;
