unit AlphaBleeder;

{$mode ObjFPC}{$H+}

interface

uses
  CastleImages;

function MakeAlphaBleed(const Src: TRGBAlphaImage): TRGBAlphaImage;
function MakeAlphaBleed(const Src: TGrayscaleAlphaImage): TGrayscaleAlphaImage;

implementation
uses
  SysUtils,
  CastleVectors, CastleUtils;

const
  Threshold = Byte(0);
  MaxDistance = MaxInt;

{$include alphableeder.inc}
{$define rgba}
{$include alphableeder.inc}

end.

