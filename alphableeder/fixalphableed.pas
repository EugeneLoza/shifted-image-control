program FixAlphaBleed;

uses
  SysUtils,
  CastleImages,
  AlphaBleeder;

var
  ImgSrc: TCastleImage;
  ImgDst: TCastleImage;
  I: Integer;
begin
  if ParamCount < 1 then
    WriteLn('Provide list of images to process.')
  else
    for I := 1 to ParamCount do
    begin
      WriteLn('Fixing Alpha Bleed: ' + ParamStr(I));
      ImgSrc := LoadImage(ParamStr(I));
      if ImgSrc is TRGBAlphaImage then
        ImgDst := MakeAlphaBleed(TRGBAlphaImage(ImgSrc))
      else
      if ImgSrc is TGrayscaleAlphaImage then
        ImgDst := MakeAlphaBleed(TGrayscaleAlphaImage(ImgSrc))
      else
      begin
        WriteLn('Image doesn''t have alpha channel, skipping');
        FreeAndNil(ImgSrc);
        continue;
      end;
      SaveImage(ImgDst, ParamStr(I));
      FreeAndNil(ImgSrc);
      FreeAndNil(ImgDst);
    end;
end.

