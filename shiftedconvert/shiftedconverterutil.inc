function CropCorners(const Src: {$ifdef rgba}TRGBAlphaImage{$else}TGrayscaleAlphaImage{$endif}): TCastleShiftedImage;
const
  AlphaChannel = {$ifdef rgba}3{$else}1{$endif};

  function Left: Integer;
  var
    X, Y, Z: Integer;
  begin
    Result := 0;
    for X := 0 to Src.Width - 1 do
      for Y := 0 to Src.Height - 1 do
        for Z := 0 to Src.Depth - 1 do
          if Src.PixelPtr(X, Y, Z)^.AsArray[AlphaChannel] > Threshold then
            Exit(X);
  end;

  function Right: Integer;
  var
    X, Y, Z: Integer;
  begin
    Result := Src.Width - 1;
    for X := Src.Width - 1 downto 0 do
      for Y := 0 to Src.Height - 1 do
        for Z := 0 to Src.Depth - 1 do
          if Src.PixelPtr(X, Y, Z)^.AsArray[AlphaChannel] > Threshold then
            Exit(X);
  end;

  function Bottom: Integer;
  var
    X, Y, Z: Integer;
  begin
    Result := 0;
    for Y := 0 to Src.Height - 1 do
      for X := 0 to Src.Width - 1 do
        for Z := 0 to Src.Depth - 1 do
          if Src.PixelPtr(X, Y, Z)^.AsArray[AlphaChannel] > Threshold then
            Exit(Y);
  end;

  function Top: Integer;
  var
    X, Y, Z: Integer;
  begin
    Result := Src.Height - 1;
    for Y := Src.Height - 1 downto 0 do
      for X := 0 to Src.Width - 1 do
        for Z := 0 to Src.Depth - 1 do
          if Src.PixelPtr(X, Y, Z)^.AsArray[AlphaChannel] > Threshold then
            Exit(Y);
  end;

var
  ShiftLeft, ShiftBottom, NewWidth, NewHeight: Integer;
  Y: Integer;
begin
  ShiftLeft := Left;
  ShiftBottom := Bottom;
  NewWidth := Right - ShiftLeft + 1;
  NewHeight := Top - ShiftBottom + 1;
  Result := TCastleShiftedImage.Create;
  Result.Image := {$ifdef rgba}TRGBAlphaImage{$else}TGrayscaleAlphaImage{$endif}.Create(NewWidth, NewHeight, Src.Depth);
  for Y := 0 to NewHeight - 1 do
    Move(Src.PixelPtr(ShiftLeft, Y + ShiftBottom, 0)^, Result.Image.PixelPtr(0, Y, 0)^, Src.PixelSize * NewWidth);
  Result.Height := Src.Height;
  Result.Width := Src.Width;
  Result.ShiftX := ShiftLeft;
  Result.ShiftY := ShiftBottom;
end;
