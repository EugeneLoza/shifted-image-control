unit ShiftedConverterUtil;

{$mode ObjFPC}{$H+}

interface

uses
  CastleImages, CastleShiftedImage;

function CropCorners(const Src: TRGBAlphaImage): TCastleShiftedImage;
function CropCorners(const Src: TGrayscaleAlphaImage): TCastleShiftedImage;

implementation
uses
  SysUtils,
  CastleVectors, CastleUtils;

const
  Threshold = Byte(0);

{$include shiftedconverterutil.inc}
{$define rgba}
{$include shiftedconverterutil.inc}

end.

