program ShiftedConverter;

uses
  SysUtils,
  CastleImages, CastleUriUtils,
  ShiftedConverterUtil, CastleShiftedImage;

var
  ImgSrc: TCastleImage;
  ImgDst: TCastleShiftedImage;
  I: Integer;
  ImageName: String;
begin
  if ParamCount < 1 then
    WriteLn('Provide list of images to process.')
  else
    for I := 1 to ParamCount do
    begin
      ImageName := FilenameToURISafe(ParamStr(I));
      WriteLn('Cropping corners: ' + ImageName);
      ImgSrc := LoadImage(ImageName);
      if ImgSrc is TRGBAlphaImage then
        ImgDst := CropCorners(TRGBAlphaImage(ImgSrc))
      else
      if ImgSrc is TGrayscaleAlphaImage then
        ImgDst := CropCorners(TGrayscaleAlphaImage(ImgSrc))
      else
      begin
        WriteLn('Image doesn''t have alpha channel, skipping');
        FreeAndNil(ImgSrc);
        continue;
      end;
      ImgDst.Save(ImageName);
      FreeAndNil(ImgSrc);
      FreeAndNil(ImgDst);
    end;
end.

