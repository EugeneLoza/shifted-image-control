# Shifted Image Control

Converter and renderer for "shifted images", i.e. raster images with large transparent areas - both to increase performance and RAM+VRAM usage of such images.

Warning: Alpha bleeding fix algorithm is extremely thorough and inefficient, that means in some cases it may take literally hours to process an image.

# License

License is the same as Castle Game Engine, unless explicitly specified, see https://castle-engine.io/license.php
